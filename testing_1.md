```plantuml

!include https://gitlab.com/arihantar/plantuml/-/raw/master/test.puml
Bob -> Alice : hello

show circle

class ImplementClass
abstract class AbstractClass
interface InterfaceClass

enum Enum {
  ONE
  TWO
  THREE
}

annotation Annotation
class CustomClass << (S,orchid) custom >>

```
